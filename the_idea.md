**Drawing graphs**      

- Use `qcustomplot.h` for drawing.      
Documentation: [http://www.qcustomplot.com/documentation/classQCustomPlot.html](http://www.qcustomplot.com/documentation/classQCustomPlot.html)      
      
**Calculating**      

- Use `NTL/RR.h` for calculating.      
Documentation: [http://www.shoup.net/ntl/doc/tour-struct.html](http://www.shoup.net/ntl/doc/tour-struct.html)      
      
**Math**      

- Use the set of equations published by В. П. Коробейников, В. И. Власов, Д. Б. Волков. You can find this set in the paragraph 3 of the publication.      
Link: [http://www.mathnet.ru/php/getFT.phtml?jrnid=mm&paperid=1897&what=fullt&option_lang=rus](http://www.mathnet.ru/php/getFT.phtml?jrnid=mm&paperid=1897&what=fullt&option_lang=rus)      

- To precise calculation, use the Runge–Kutta four-order method.     
[Descriptions](http://www.phy.davidson.edu/FacHome/dmb/py200/RungeKuttaMethod.htm) [here](http://www.uchites.ru/files/nummethod_book_chapter4-1.pdf)    
    
**Interface elements**      

- input the initial values:      
```      
    V -- speed      
    m -- mass --------------------->| R -- radius      
    ρ -- object density (const) --->|      
    α -- angle      
    H -- height      
    cx -- drag coefficient (0.843 or 1.0 -- learn better) (const)      
    g -- gravity acceleration (const)      
    σ -- ablation coefficient (const)      
```      

- switch between ρ(H) dependence mode: Earth and Jupiter currently avail. (?)    

- two columns of quantities to select the dependence:      
```  
    H -- height      
    W -- width      
    t -- time      
    m -- mass      
    V -- speed      
    α -- angle      
    S -- trajectory length      
```  

- big graph, particularly      
      
**Classes**      

- Evaluate  

- Interface  
  
**Interface behaviour**

- On click "Compute':  
    + computations are being made in a thread;  
    + in statusbar there is the next message: `state: computing...`  
    + then `state: ready to draw`  

- `^S` opens a little window with image characteristics (fields X and Y are associated: change one, another changes itself) and button `Save`  

- `^[` aborts computation; in statusbar: `state: aborted`  

- correct the tab-focusing order  

- navigate through the fields also by Enter and arrow keys.  

- think about when re-computing: we should erase all the existing data.  

- redraw the graph right when Y-axis or X-axis value changed. somehow let altitude & width be by default.  
  
* * *  
  
*To do in the indeterminable future:*      
**Data saving**      
Automatically save the calculated data in files, names of that are generated based on the given parameters so that you can definitely associate the file with the parameters. That feature is to avoid re-calculating.      
