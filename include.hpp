#ifndef INCLUDES_H
#define INCLUDES_H

#define QCUSTOMPLOT_USE_LIBRARY
#include <qcustomplot.h>
#include <NTL/RR.h>
#include <vector>
#include <cmath>
#include <iostream>
#include <string>
#include <boost/thread/thread.hpp>
#include <boost/bind/bind.hpp>

/*
inline void ifOK()
{
    std::cerr << __FILE__ << ": on line " << __LINE__ << "\n";
}
*/

#define ifOK() std::cerr << __FILE__ << ": on line " << __LINE__ << "\n"

#endif // INCLUDES_H
