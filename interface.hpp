#ifndef INTERFACE_HPP
#define INTERFACE_HPP

#include <QMainWindow>
#include "include.hpp"

using std::vector;
using std::string;
using NTL::RR;
using boost::thread;

namespace Ui {
class Interface;
}

class Interface : public QMainWindow
{
    Q_OBJECT

public:
    explicit Interface(QWidget *parent = 0);
    ~Interface();

private slots:
    void on_button_compute_clicked();

private:
    Ui::Interface *ui;
    vector<RR> H, V, m, alpha, t, W, S;

    void compute( RR* arg, vector<RR>** store_here );
    void draw();

    RR RRconv( double );
    double doubleconv( RR );

};

#endif // INTERFACE_HPP
