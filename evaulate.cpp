#include "evaulate.hpp"

Evaulate::Evaulate()
{
    ;
}

Evaulate* Evaulate::Init( RR _V, RR _m, RR _rho, RR _alpha, RR _H, RR _cx, RR _g, RR _sigma, RR _dH, RR _R )
{
    if ( !exists_already )
    {
        V[0] = _V;
        m[0] = _m;
        rho = _rho;
        alpha[0] = degrees_to_radians( _alpha );
        H[0] = _H;
        W[0] = 0;
        S[0] = 0;
        cx = _cx;
        g = _g;
        sigma = _sigma;
        dh = _dH;
        R = _R;

        exists_already = true;
        existing_instance = this;
        return existing_instance;
    }
    else
    {
        cerr << "Sorry. One instance of this singletone class already exists. To re-use this class with new parameters, you should delete that instanse, then create a new one.\n";
        return existing_instance;
    }
}

RR Evaulate::area( RR m )
{
    return power( 3 * m / ( 4 * NTL::ComputePi_RR() * rho ), 1 / 3 );
}

RR Evaulate::areaR( RR R )
{
    return NTL::ComputePi_RR() * power( R, 2 );
}

RR Evaulate::density_Earth( RR H )
{
    const RR density_at_zero( 1.29 * pow( 10, -3 ) ); // g / cm^3
    const RR H_what( 7.5 ); // km

    return density_at_zero * exp( -H / H_what );
}

RR Evaulate::degrees_to_radians( RR degrees )
{
    return degrees / 180 * NTL::ComputePi_RR();
}

RR Evaulate::dVH_per_dH( RR H, RR V, RR m, RR alpha, RR t )
{
    return cx / 2.0 * density_Earth( H ) * area( m ) * V / ( m * conv<RR>( sin( conv<double>( alpha ) ) ) ) - g / V;
}

RR Evaulate::dmH_per_dH( RR H, RR V, RR m, RR alpha, RR t )
{
    return sigma / 2.0 * density_Earth( H ) * area( m ) * power( V, 2 ) / conv<RR>( sin( conv<double>( alpha ) ) );
}

RR Evaulate::dalphaH_per_dH( RR H, RR V, RR m, RR alpha, RR t )
{
    return 0 + ( 1.0 / R  - g / power( V, 2 ) ) * ( conv<RR>( cos( conv<double>(alpha) ) ) / conv<RR>( sin( conv<double>(alpha) ) ) );
}

RR Evaulate::dtH_per_dH( RR H, RR V, RR m, RR alpha, RR t )
{
    return -1.0 / ( V * conv<RR>( sin( conv<double>(alpha) ) ) );
}

template< typename T >
T* Evaulate::rk4_system4( T h, T dh
                        , T k, T K( T, T, T, T, T )
                        , T l, T L( T, T, T, T, T )
                        , T m, T M( T, T, T, T, T )
                        , T n, T N( T, T, T, T, T ) )
{
    T k1, k2, k3, k4,
      l1, l2, l3, l4,
      m1, m2, m3, m4,
      n1, n2, n3, n4,
      h1, h2, h3, h4;

    h1 = 0.0;
    h2 = dh / 2.0;
    h3 = dh / 2.0;
    h4 = dh;

    k1 = K( h + h1, k, l, m, n ) * dh;
    l1 = L( h + h1, k, l, m, n ) * dh;
    m1 = M( h + h1, k, l, m, n ) * dh;
    n1 = N( h + h1, k, l, m, n ) * dh;

    k2 = K( h + h2, k + k1 / 2.0, l + l1 / 2.0, m + m1 / 2.0, n + n1 / 2.0) * dh;
    l2 = L( h + h2, k + k1 / 2.0, l + l1 / 2.0, m + m1 / 2.0, n + n1 / 2.0) * dh;
    m2 = M( h + h2, k + k1 / 2.0, l + l1 / 2.0, m + m1 / 2.0, n + n1 / 2.0) * dh;
    n2 = N( h + h2, k + k1 / 2.0, l + l1 / 2.0, m + m1 / 2.0, n + n1 / 2.0) * dh;

    k3 = K( h + h3, k + k2 / 2.0, l + l2 / 2.0, m + m2 / 2.0, n + n2 / 2.0) * dh;
    l3 = L( h + h3, k + k2 / 2.0, l + l2 / 2.0, m + m2 / 2.0, n + n2 / 2.0) * dh;
    m3 = M( h + h3, k + k2 / 2.0, l + l2 / 2.0, m + m2 / 2.0, n + n2 / 2.0) * dh;
    n3 = N( h + h3, k + k2 / 2.0, l + l2 / 2.0, m + m2 / 2.0, n + n2 / 2.0) * dh;

    k4 = K( h + h4, k + k3, l + l3, m + m3, n + n3 ) * dh;
    l4 = L( h + h4, k + k3, l + l3, m + m3, n + n3 ) * dh;
    m4 = M( h + h4, k + k3, l + l3, m + m3, n + n3 ) * dh;
    n4 = N( h + h4, k + k3, l + l3, m + m3, n + n3 ) * dh;

    T* a = new T[5]; //answer
    a[0] = h + dh;
    a[1] = k + ( k1 + 2.0 * k2 + 2.0 * k3 + k4 ) / 6.0;
    a[2] = l + ( l1 + 2.0 * l2 + 2.0 * l3 + l4 ) / 6.0;
    a[3] = m + ( m1 + 2.0 * m2 + 2.0 * m3 + m4 ) / 6.0;
    a[4] = n + ( n1 + 2.0 * n2 + 2.0 * n3 + n4 ) / 6.0;

    return a;
}

void Evaulate::next_iteration( RR dH )
{
    RR* a = new RR[5];
    a = rk4_system4( H.back(), dH,
                     V.back(), dVH_per_dH,
                     m.back(), dmH_per_dH,
                     alpha.back(), dmH_per_dH,
                     t.back(), dtH_per_dH );
    RR H_change = a[0] - H.back();
    H.push_back( a[0] );
    V.push_back( a[1] );
    m.push_back( a[2] );
    alpha.push_back( a[3] );
    t.push_back( a[4] );

    RR W_change = conv<RR>( cos( conv<double>( alpha.back() ) ) ) * V.back();
    W.push_back( W.back() + W_change );
    S.push_back( conv<RR>( sqrt( conv<double>( power( H_change, 2 ) + power( W_change, 2 ) ) ) ) );

    delete[] a;
}

void Evaulate::evaulate()
{
    while( H.back() > 0.0 )
    {
        next_iteration( dh );
    }
}

// Initialize variables

vector<RR> Evaulate::V, Evaulate::m, Evaulate::alpha, Evaulate::W, Evaulate::t, Evaulate::S, Evaulate::H;
RR Evaulate::cx, Evaulate::g, Evaulate::sigma, Evaulate::rho, Evaulate::R, Evaulate::dh;

bool Evaulate::exists_already;
Evaulate* Evaulate::existing_instance;
