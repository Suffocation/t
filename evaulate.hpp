#ifndef EVAULATE_HPP
#define EVAULATE_HPP

#include "includes.hpp"

using NTL::RR;
using NTL::conv;
using std::vector;
using std::cerr;

class Evaulate
{
public:
    Evaulate();
    Evaulate* Init( RR _V, RR _m, RR _rho, RR _alpha, RR _H, RR _cx, RR _g, RR _sigma, RR _dH, RR _R );
    ~Evaulate();
    void evaulate();
    static vector<RR> H, V, m, alpha, t, W, S;
    static RR cx, g, sigma, rho, R, dh;
private:
    static RR density_Earth( RR H ); // air density
    static RR area( RR m );
    static RR areaR( RR R );

    static RR degrees_to_radians( RR );

    void next_iteration( RR dh );

    template< typename T >
    T* rk4_system4( T h, T dh
                  , T k, T K( T, T, T, T, T )
                  , T l, T L( T, T, T, T, T )
                  , T m, T M( T, T, T, T, T )
                  , T n, T N( T, T, T, T, T ) ); // returns RR[4]

    static RR dVH_per_dH( RR H, RR V, RR m, RR alpha, RR t ); // dV(H)/Dh
    static RR dmH_per_dH( RR H, RR V, RR m, RR alpha, RR t ); // dm(H)/dH
    static RR dalphaH_per_dH( RR H, RR V, RR m, RR alpha, RR t ); // d alpha(H) / dH
    static RR dtH_per_dH( RR H, RR V, RR m, RR alpha, RR t ); // dt(H) / dH

    static bool exists_already;
    static Evaulate* existing_instance;
};

#endif // EVAULATE_HPP
