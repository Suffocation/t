# Just the very first edition

OTHER_FILES += \
    the_idea.md

HEADERS += \
    evaulate.hpp \
    interface.hpp \
    include.hpp

SOURCES += \
    evaulate.cpp \
    main.cpp \
    interface.cpp

FORMS += \
    interface.ui

INCLUDEPATH += /usr/include/x86_64-linux-gnu/qt5/QtWidgets/

QT += widgets
greaterThan(QT_MAJOR_VERSION, 4): QT += printsupport

LIBS += -lntl -lqcustomplot -lboost_thread -lboost_system
