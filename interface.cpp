#include "interface.hpp"
#include "ui_interface.h"
#include "evaulate.hpp"

Interface::Interface(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Interface)
{
    ifOK();

    ui->setupUi(this);

    ui->statusbar->showMessage( "state: I undertood!", 4000 );
    ui->list_dependencies->item( 0 )->setSelected( true ); // altitude
    ui->list_dependencies2->item( 1 )->setSelected( true ); // weidth
    setTabOrder( ui->line_V, ui->line_m );
    setTabOrder( ui->line_m, ui->line_alpha );
    setTabOrder( ui->line_alpha, ui->line_H );
    setTabOrder( ui->line_H, ui->line_cx );
    setTabOrder( ui->line_cx, ui->line_g);
    setTabOrder( ui->line_g, ui->line_sigma);
    setTabOrder( ui->line_sigma, ui->line_rho );
    setTabOrder( ui->line_rho, ui->line_R );
    setTabOrder( ui->line_R, ui->line_dh );
    setTabOrder( ui->line_rho, ui->box_atmosphere_mode );
    setTabOrder( ui->box_atmosphere_mode, ui->button_compute );
    setTabOrder( ui->button_compute, ui->list_dependencies );
    setTabOrder( ui->list_dependencies, ui->list_dependencies2 );

    ui->line_V->setButtonSymbols( QAbstractSpinBox::NoButtons ); // After this it looks just like a QLineEdit.
    ui->line_m->setButtonSymbols( QAbstractSpinBox::NoButtons );
    ui->line_alpha->setButtonSymbols( QAbstractSpinBox::NoButtons );
    ui->line_H->setButtonSymbols( QAbstractSpinBox::NoButtons );
    ui->line_cx->setButtonSymbols( QAbstractSpinBox::NoButtons );
    ui->line_g->setButtonSymbols( QAbstractSpinBox::NoButtons );
    ui->line_sigma->setButtonSymbols( QAbstractSpinBox::NoButtons );
    ui->line_rho->setButtonSymbols( QAbstractSpinBox::NoButtons );
    ui->line_R->setButtonSymbols( QAbstractSpinBox::NoButtons );
    ui->line_dh->setButtonSymbols( QAbstractSpinBox::NoButtons );

    ifOK();
    QObject::connect( ui->button_compute, SIGNAL( clicked() ), this, SLOT( on_button_compute_clicked() ) );
    ifOK();
}

Interface::~Interface()
{
    delete ui;
}

RR Interface::RRconv( double value )
{
    return conv< RR >( value );
}

double Interface::doubleconv( RR value )
{
    return conv< double >( value );
}

void Interface::on_button_compute_clicked()
{
    ui->button_compute->setDisabled( true );
    vector<RR>** store_here = new vector<RR>*[7];
    ifOK();
    store_here[0] = &H;
    store_here[0] = &V;
    store_here[0] = &m;
    store_here[0] = &alpha;
    store_here[0] = &t;
    store_here[0] = &W;
    store_here[0] = &S;

    RR arg[10] = { RRconv( ui->line_V->value() ),
                   RRconv( ui->line_m->value() ),
                   RRconv( ui->line_rho->value() ),
                   RRconv( ui->line_alpha->value() ),
                   RRconv( ui->line_H->value() ),
                   RRconv( ui->line_cx->value() ),
                   RRconv( ui->line_g->value() ),
                   RRconv( ui->line_sigma->value() ),
                   RRconv( ui->line_dh->value() ),
                   RRconv( ui->line_R->value() ) };
    ifOK();

    boost::thread t( &Interface::compute, this,
                     arg,
                     store_here );
    ifOK();

    ui->button_compute->setDisabled( false );
}

void Interface::compute( RR* arg, vector<RR>** store_here )
{
    ifOK();
    Evaulate* e = new Evaulate;
    ifOK();
    e->Init( arg[0], arg[1], arg[2], arg[3], arg[4], arg[5], arg[6], arg[7], arg[8], arg[9], store_here );
    ifOK();
    ui->statusbar->showMessage( "state: computing...", 0 );
    ifOK();
    e->evaulate();
    ifOK();
    ui->statusbar->showMessage( "state: drawing...", 0 );
    ifOK();
    draw();
    ui->statusbar->showMessage( "state: done", 0 );
    ifOK();
    delete e;
}

void Interface::draw()
{
    ;
}
